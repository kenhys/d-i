# Georgian messages for ubuntu-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Aiet Kolkhi <aietkolkhi@gmail.com>, 2005, 2006, 2007, 2008.
#
# This file is maintained by Aiet Kolkhi <aietkolkhi@gmail.com>
#
# Includes contributions by Malkhaz Barkalaza <malxaz@gmail.com>,
# Alexander Didebulidze <didebuli@in.tum.de>, Vladimer Sichinava <vlsichinava@gmail.com>
# Taya Kharitonashvili <taya13@gmail.com>
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer.2006071\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-11 20:02+0000\n"
"PO-Revision-Date: 2024-09-17 23:09+0000\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 5.8-dev\n"

#. Type: boolean
#. Description
#. :sl5:
#: ../apt-mirror-setup.templates:2001
msgid "Use non-free firmware?"
msgstr "გამოვიყენო არა-თავისუფალი მიკროკოდი?"

#. Type: boolean
#. Description
#. :sl5:
#: ../apt-mirror-setup.templates:2001
msgid ""
"Firmware is a kind of software providing low-level control of certain "
"hardware components (such as Wi-Fi cards or audio chipsets), which may not "
"function fully or at all without it."
msgstr ""
"მიკროკოდი პროგრამის ტიპია ზოგიერთი აპარატურული კომპონენტის (მაგ: Wi-Fi "
"ბარათები ან აუდიო ჩიპები) ქვედა დონის მართვისთვის, რომლებიც ისე არ "
"იმუშავებდნენ."

#. Type: boolean
#. Description
#. :sl5:
#: ../apt-mirror-setup.templates:2001
msgid ""
"Although not at all part of Debian, some non-free firmware has been made to "
"work with Debian. This firmware has varying licenses which restrict your "
"freedoms to use, modify, or share the software, and generally does not have "
"source forms that you may study."
msgstr ""
"მიუხედავად მისა, რომ ის Debian-ის ნაწილი არაა, ზოგიერთი არათავისუფალი "
"მიკროკოდი Debian-ზე მაინც აამუშავებს. ამ მიკროკოდების ლიცენზია იცვლება და "
"ზღუდავს თქვენს თავისუფლებას, გამოიყენოთ, შეცვალოთ ან გააზიაროთ ეს პროგრამა "
"და, როგორც წესი, არ აქვს პროგრამული კოდი, რომელიც შეგიძლიათ, შეისწავლოთ."

#. Type: text
#. Description
#. :sl5:
#: ../partman-base.templates:58001
#, no-c-format
msgid "ZFS pool %s, volume %s"
msgstr "ZFS პული %s, ტომი %s"

#. Type: text
#. Description
#. :sl5:
#: ../partman-base.templates:60001
#, no-c-format
msgid "DASD %s (%s)"
msgstr "DASD %s (%s)"

#. Type: text
#. Description
#. :sl5:
#: ../partman-base.templates:61001
#, no-c-format
msgid "DASD %s (%s), partition #%s"
msgstr "DASD %s (%s), დანაყოფი #%s"

#. Type: boolean
#. Description
#. :sl5:
#: ../partman-basicfilesystems.templates:60001
msgid ""
"Your boot partition has not been configured with the ext2 file system. This "
"is needed by your machine in order to boot. Please go back and use the ext2 "
"file system."
msgstr ""
"თქვენ ჩატვირთვადი დანაყოფი არაა ext2 ფაილური სისტემით დაყენებული. ეს "
"აუცილებელია თქვენი სისტემის ჩატვირთვისთვის. გთხოვთ დაბრუნდეთ და გამოიყენოთ "
"ext2 ფაილური სისტემა."

#. Type: boolean
#. Description
#. :sl5:
#: ../partman-basicfilesystems.templates:61001
msgid ""
"Your boot partition is not located on the first partition of your hard disk. "
"This is needed by your machine in order to boot.  Please go back and use "
"your first partition as a boot partition."
msgstr ""
"თქვენი ჩატვირთვადი დანაყოფი თქვენი მყარი დისკის პირველ დანაყოფზე არაა. ეს "
"საჭიროა, რომ თქვენი მანქანა ჩაიტვირთოს.     დაბრუნდით და გამოიყენეთ პირველი "
"დანაყოფი, როგორც ჩასატვირთი."

#. Type: text
#. Description
#. :sl5:
#. Setting to reserve a small part of the disk for use by BIOS-based bootloaders
#. such as GRUB.
#: ../partman-partitioning.templates:32001
msgid "Reserved BIOS boot area"
msgstr "დაცული BIOS-ის ჩატვირთვის ტერიტორია"

#. Type: text
#. Description
#. :sl5:
#. short variant of 'Reserved BIOS boot area'
#. Up to 10 character positions
#: ../partman-partitioning.templates:33001
msgid "biosgrub"
msgstr "biosgrub"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. :sl5:
#: ../s390-netdevice.templates:1001
msgid "ctc: Channel to Channel (CTC) or ESCON connection"
msgstr "ctc: არხიდან არხამდე (CTC) ან ESCON მიერთება"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. :sl5:
#: ../s390-netdevice.templates:1001
msgid "qeth: OSA-Express in QDIO mode / HiperSockets"
msgstr "qeth: OSA-Express QDIO რეჟიმში / HiperSockets"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. :sl5:
#: ../s390-netdevice.templates:1001
msgid "iucv: Inter-User Communication Vehicle - available for VM guests only"
msgstr "iucv: Inter-User Communication Vehicle - მხოლოდ VM სტუმრებისათვის"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. :sl5:
#: ../s390-netdevice.templates:1001
msgid "virtio: KVM VirtIO"
msgstr "virtio: KVM VirtIO"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:1002
msgid "Network device type:"
msgstr "ქსელური მოწყობილობის ტიპი:"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:1002
msgid ""
"Please choose the type of your primary network interface that you will need "
"for installing the Debian system (via NFS or HTTP). Only the listed devices "
"are supported."
msgstr ""
"გთხოვთ მიუთითოთ თქვენი ძირითადი ქსელის ტიპი, რომელიც გამოიყენება Debian-ის "
"სისტემის დაყენებისთვის (NFS ან HTTP-ის გამოყენებით). მხარდაჭერილია მხოლოდ "
"ნაჩვენები მოწყობილობები."

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:2001
msgid "CTC read device:"
msgstr "CTC წაკითხვის მოწყობილობა:"

#. Type: select
#. Description
#. :sl5:
#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:2001 ../s390-netdevice.templates:3001
msgid "The following device numbers might belong to CTC or ESCON connections."
msgstr ""
"შემდეგი მოწყობილობის ნომრები, შესაძლოა, CTC ან ESCON კავშირებს ეკუთვნოდეს."

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:3001
msgid "CTC write device:"
msgstr "CTC ჩაწერის მოწყობილობა:"

#. Type: boolean
#. Description
#. :sl5:
#. Type: boolean
#. Description
#. :sl5:
#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:4001 ../s390-netdevice.templates:8001
#: ../s390-netdevice.templates:12001
msgid "Do you accept this configuration?"
msgstr "იღებთ ამ კონფიგურაციას?"

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:4001
msgid ""
"The configured parameters are:\n"
" read channel  = ${device_read}\n"
" write channel = ${device_write}\n"
" protocol      = ${protocol}"
msgstr ""
"მითითებულია პარამეტრები:\n"
" read channel  = ${device_read}\n"
" write channel = ${device_write}\n"
" protocol      = ${protocol}"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:5001
msgid "No CTC or ESCON connections"
msgstr "CTC ან ESCON მიერთებების გარეშე"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:5001
msgid "Please make sure that you have set them up correctly."
msgstr "დარწმუნდით, რომ პარამეტრები სწორია."

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:6001
msgid "Protocol for this connection:"
msgstr "პროტოკოლი ამ შეერთებისთვის:"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:7001
msgid "Device:"
msgstr "მოწყობილობა:"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:7001
msgid "Please select the OSA-Express QDIO / HiperSockets device."
msgstr "აირჩიეთ OSA-Express QDIO / HiperSockets მოწყობილობა."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:8001
msgid ""
"The configured parameters are:\n"
" channels = ${device0}, ${device1}, ${device2}\n"
" port     = ${port}\n"
" layer2   = ${layer2}"
msgstr ""
"მითითებულია პარამეტრები:\n"
" channels = ${device0}, ${device1}, ${device2}\n"
" port     = ${port}\n"
" layer2   = ${layer2}"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:9001
msgid "No OSA-Express QDIO cards / HiperSockets"
msgstr "OSA-Express QDIO ბარათების / HiperSocket-ების გარეშე"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:9001
msgid ""
"No OSA-Express QDIO cards / HiperSockets were detected. If you are running "
"VM please make sure that your card is attached to this guest."
msgstr ""
"ვერ მოიძებნა OSA-Express QDIO cards / HiperSockets. თუ თქვენ VM-ს იყენებთ, "
"დარწმუნდით, რომ თქვენი ბარათი ამ სტუმარზე მიერთებულია."

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:10001
msgid "Port:"
msgstr "პორტი:"

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:10001
msgid "Please enter a relative port for this connection."
msgstr "შეიყვანეთ შედარებითი პორტი ამ მიერთებისთვის."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:11001
msgid "Use this device in layer2 mode?"
msgstr "გამოვიყენო ეს მოწყობილობა რეჟიმში layer2?"

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:11001
msgid ""
"By default OSA-Express cards use layer3 mode. In that mode LLC headers are "
"removed from incoming IPv4 packets. Using the card in layer2 mode will make "
"it keep the MAC addresses of IPv4 packets."
msgstr ""
"სტანდარტულად OSA-Express ბარათები layer3 რეჟიმს იყენებს. ამ რეჟიმში LLC "
"თავსართები შემომავალ IPv4 პაკეტებს შორდება. ბარათის layer2 რეჟიმში "
"გამოყენება შეუნარჩუნებს მას IPv4 პაკეტების MAC-მისამართებს."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:12001
msgid ""
"The configured parameter is:\n"
" peer  = ${peer}"
msgstr ""
"მითითებული პარამეტრია:\n"
" peer  = ${peer}"

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:13001
msgid "VM peer:"
msgstr "VM პარტნიორი:"

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:13001
msgid "Please enter the name of the VM peer you want to connect to."
msgstr "შეიყვანეთ პარტნიორი VM-ის სახელი, რომელსაც გნებავთ, მიუერთდეთ."

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:13001
msgid ""
"If you want to connect to multiple peers, separate the names by colons, e."
"g.  tcpip:linux1."
msgstr ""
"თუ მრავალ პარტნიორთან გსურთ მიერთები, გამოყავით სახელები ორწერტილით, მაგ. "
"tcpip:linux1."

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:13001
msgid ""
"The standard TCP/IP server name on VM is TCPIP; on VIF it's $TCPIP. Note: "
"IUCV must be enabled in the VM user directory for this driver to work and it "
"must be set up on both ends of the communication."
msgstr ""
"სტანდარტული TCP/IP სერვერის სახელი ვმ-ზე არის TCPIP; VIF-ზე კი $TCPIP. "
"შენიშვნა: ამ დრაივერის სამუშაოდ IUCV გააქტიურებული უნდა იყოს VM user "
"directory-ში, ასევე იგი კომუნიკაციის ორივე ბოლოში უნდა იყოს დაყენებული."

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl5:
#: ../s390-netdevice.templates:14001
msgid "Configure the network device"
msgstr "ქსელური მოწყობილობის მორგება"

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid "Available devices:"
msgstr "ხელმისაწვდომი მოწყობილობები:"

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid ""
"The following direct access storage devices (DASD) are available. Please "
"select each device you want to use one at a time."
msgstr ""
"ხელმისაწვდომია შემდეგი „direct access storage devices“ (DASD) მოწყობილობები. "
"გთხოვთ სათითაოდ ამოირჩიოთ ყოველი გამოსაყენებელი მოწყობილობა."

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid "Select \"Finish\" at the bottom of the list when you are done."
msgstr "როცა დაასრულებთ, ქვემოთ, ღილაკს \"დასრულება\" დააწექით."

#. Type: string
#. Description
#. :sl5:
#: ../s390-dasd.templates:2001
msgid "Choose device:"
msgstr "აირჩიეთ მოწყობილობა:"

#. Type: string
#. Description
#. :sl5:
#: ../s390-dasd.templates:2001
msgid ""
"Please choose a device. You have to specify the complete device number, "
"including leading zeros."
msgstr ""
"აირჩიეთ მოწყობილობა. უნდა მიუთითოთ მოწყობილობის სრული ნომერი, საწყისი "
"ნულების ჩათვლით."

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:3001
msgid "Invalid device"
msgstr "არასწორი მოწყობილობა"

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:3001
msgid "An invalid device number has been chosen."
msgstr "არჩეულია არასწორი მოწყობილობების რაოდენობა."

#. Type: boolean
#. Description
#. :sl5:
#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001 ../s390-dasd.templates:5001
msgid "Format the device?"
msgstr "დავაფორმატო მოწყობილობა?"

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001
msgid "The DASD ${device} is already low-level formatted."
msgstr "DASD ${device} უკვე დაფორმატებულია დაბალ დონეზე."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001
msgid ""
"Please choose whether you want to format again and remove any data on the "
"DASD. Note that formatting a DASD might take a long time."
msgstr ""
"აირჩიეთ, გნებავთ თუ არა DASD-ის თავიდან დაფორმატება და მასზე მონაცემების "
"წაშლა. გაითვალისწინეთ, რომ DASD-ის დაფორმატებას კაი ხანი შეიძლება, დასჭირდეს."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:5001
msgid ""
"The DASD ${device} is not low-level formatted. DASD devices must be "
"formatted before you can create partitions."
msgstr ""
"DASD ${device} ახლა დაფორმატებულია. დანაყოფების შექმნამდე DASD მოწყობილობის "
"დაფორმატება აუცილებელია."

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:6001
msgid "The DASD ${device} is in use"
msgstr "DASD ${device} უკვე გამოიყენება"

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:6001
msgid ""
"Could not low-level format the DASD ${device} because the DASD is in use.  "
"For example, the DASD could be a member of a mapped device in an LVM volume "
"group."
msgstr ""
"DASD მოწყობილობის ${device} დაბალი დონის ფორმატირება შეუძლებელია, რადგან "
"DASD უკვე გამოიყენება.  შეიძლება DASD-ი მიმაგრებული მოწყობილობის ან LVM "
"ტომების ჯგუფის ნაწილია."

#. Type: text
#. Description
#. :sl5:
#: ../s390-dasd.templates:7001
msgid "Formatting ${device}..."
msgstr "${device}-ის დაფორმატება..."

#. Type: text
#. Description
#. Main menu item. Keep translations below 55 columns
#. :sl5:
#: ../s390-dasd.templates:8001
msgid "Configure direct access storage devices (DASD)"
msgstr "პირდაპირი წვდომის საცავის მოწყობილობების (DASD) მორგება"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl5:
#: ../zipl-installer.templates:1001
msgid "Install the ZIPL boot loader on a hard disk"
msgstr "ZIPL ჩამტვირთავის დაყენება მყარ დისკზე"
